package com.andrew.vlad.timofeevchub;


import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by student on 24.03.16.
 */
public class SingleAuto extends Activity {

    String[] stringNames = {"BMW","Audi","Nissan","Opel","Mercedes","Aston Martin"};

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_auto);
        //rest of the code
        Bundle bundle = getIntent().getExtras();
        Integer message = bundle.getInt("index");

        TextView txtView = (TextView) findViewById(R.id.elementTitle);

        String title = stringNames[message];

        txtView.setText(title);
    }

}
