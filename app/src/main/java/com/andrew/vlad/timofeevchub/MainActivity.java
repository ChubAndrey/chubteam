package com.andrew.vlad.timofeevchub;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.app.Activity;

public class MainActivity extends Activity {

    String[] names = {"BMW","Audi","Nissan","Opel","Mercedes","Aston Martin"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ListView listView = (ListView) findViewById(R.id.list_item);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, names);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object listItem = listView.getItemAtPosition(position);

                Intent newActivity = new Intent(MainActivity.this, SingleAuto.class);
                newActivity.putExtra("index", position);
                startActivity(newActivity);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public void onAddClick(View view) {

        Intent newActivity = new Intent(MainActivity.this, addAuto.class);
        startActivity(newActivity);
    }

    public void onAddClickClick(View view) {
        String string = new String();
    }

    public void addPhotoClicked(View view) {

    }
}
